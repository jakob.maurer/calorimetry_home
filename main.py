from functions import m_json
from functions import m_pck

path = ""
#metadata = m_json.get_metadata_from_setup(path)
m_pck.check_sensors()
m_json.get_metadata_from_setup("datasheets/setup_heat_capacity.json")
t = m_json.get_metadata_from_setup("datasheets/setup_heat_capacity.json")
t = m_json.add_temperature_sensor_serials("datasheets",t)
M = m_pck.get_meas_data_calorimetry(t)
m_pck.logging_calorimetry(M, t,"archive/capacity","datasheets")
m_json.archiv_json("datasheets","datasheets/setup_heat_capacity.json" , "archive/capacity")
